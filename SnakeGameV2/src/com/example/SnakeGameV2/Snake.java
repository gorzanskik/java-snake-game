package com.example.SnakeGameV2;

import java.awt.EventQueue;
import javax.swing.JFrame;

public class Snake extends JFrame {

    public Snake() {

        initUI();
    }

    private void initUI() {

        add(new Board());   //stworzenie nowego obiektu tablica

        setResizable(false);    //staly rozmiar ramki
        pack(); //dopasowanie rozmiaru ramki

        setTitle("Snake Game"); //tytul gry
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public static void main(String[] args) {

        EventQueue.invokeLater(() -> {
            JFrame ex = new Snake(); //nowa gra
            ex.setVisible(true);
        });
    }
}