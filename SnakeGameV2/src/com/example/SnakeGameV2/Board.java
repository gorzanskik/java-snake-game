package com.example.SnakeGameV2;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.Timer;

public class Board extends JPanel implements ActionListener {

    private final int B_WIDTH = 300;    //szerokosc tablicy
    private final int B_HEIGHT = 300;   //wysokosc tablicy
    private final int DOT_SIZE = 10;    //rozmiar obiektu
    private final int ALL_DOTS = 900;   //makszymalna ilosc kropek
    private final int RAND_POS = 29;    //pomocniczna wartosc do liczenia miejsca dla jablka
    private final int DELAY = 150;  //szybkosc gry

    private final int x[] = new int[ALL_DOTS];  //wspolrzedne polaczen weza
    private final int y[] = new int[ALL_DOTS];

    private int dots;   //aktualny rozmiar weza
    private int apple_x;    //polozenie jablka
    private int apple_y;

    //kierunki ruchu weza
    private boolean GoLeft = false;
    private boolean GoRight = true;  //na poczatku porusza sie w prawo
    private boolean GoUp = false;
    private boolean GoDown = false;
    private boolean inGame = true; //gra trwa

    private Timer timer;
    private Image ball;
    private Image apple;
    private Image head;

    public Board() {

        initBoard();    //tworzenie tablicy
    }

    private void initBoard() {  //tworzenie tablicy o okreslonym kolorze i rozmiarze

        addKeyListener(new TAdapter()); //obsluga klawiszy
        setBackground(Color.black);     //czarny kolor tla
        setFocusable(true);

        setPreferredSize(new Dimension(B_WIDTH, B_HEIGHT)); //ustawienie rozmiaru
        loadImages();   //zaladowanie obrazkow
        initGame();     //inicjalizacja gry
    }

    private void loadImages() {     //pobranie obrazkow

        ImageIcon iid = new ImageIcon("src/resources/dot.png"); //kulka
        ball = iid.getImage();

        ImageIcon iia = new ImageIcon("src/resources/apple.png");   //jablko
        apple = iia.getImage();

        ImageIcon iih = new ImageIcon("src/resources/head.png");    //glowa weza
        head = iih.getImage();
    }

    private void initGame() {   //tworzenie gry

        dots = 3;   //poczatkowy rozmiar weza

        for (int z = 0; z < dots; z++) {    //wspolrzedne w ktorych ma sie pojawic waz
            x[z] = 50 - z * 10;         //x[0], y[0] - glowa weza
            y[z] = 50;
        }

        locateApple();  //losowe ustawienie jablka na tablicy

        timer = new Timer(DELAY, this);
        timer.start();  //start timera
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        doDrawing(g);
    }

    private void doDrawing(Graphics g) {

        if (inGame) {   //jesli gra trwa

            g.drawImage(apple, apple_x, apple_y, this); //rysowanie jablka w podanym miejscu

            for (int z = 0; z < dots; z++) {
                if (z == 0) {   // -> z=0 -> glowa weza
                    g.drawImage(head, x[z], y[z], this);    //rysowanie glowy w podanym miejscu
                } else {
                    g.drawImage(ball, x[z], y[z], this);    //rysowanie fragmentow weza w podanych miejscach
                }
            }

            Toolkit.getDefaultToolkit().sync();

        } else {

            gameOver(g); //gra skonczona
        }
    }

    private void gameOver(Graphics g) { //zakonczenie gry -> komunikat game over o okreslonych parametrach

        String msg = "Game Over";
        Font small = new Font("Helvetica", Font.BOLD, 14);
        FontMetrics metr = getFontMetrics(small);

        g.setColor(Color.white);
        g.setFont(small);
        g.drawString(msg, (B_WIDTH - metr.stringWidth(msg)) / 2, B_HEIGHT / 2);
    }

    private void checkApple() { //sprawdzenie czy waz zjadl jablko

        if ((x[0] == apple_x) && (y[0] == apple_y)) {   //jesli wspolrzedne glowy weza i jablka sa takie same

            dots++; //zwiekszenie dlugosci weza
            locateApple();  //nowa lokacja jablka
        }
    }

    private void move() {   //poruszanie sie kolejnych fragmentow weza

        for (int z = dots; z > 0; z--) {    //przesuwanie fragmentow weza w gore lancucha
            x[z] = x[(z - 1)];
            y[z] = y[(z - 1)];
        }

        if (GoLeft) {       //przesuniecie glowy w lewo
            x[0] -= DOT_SIZE;
        }

        if (GoRight) {      //przesuniecie glowy w prawo
            x[0] += DOT_SIZE;
        }

        if (GoUp) {         //przesuniecie glowy w gore
            y[0] -= DOT_SIZE;
        }

        if (GoDown) {       //przesuniecie glowy w dol
            y[0] += DOT_SIZE;
        }
    }

    private void checkCollision() { //sprawdzenie czy waz uderzyl w konce tablicy lub sam w siebie

        for (int z = dots; z > 0; z--) {    //gdy waz uderzy sam w siebie

            if ((z > 4) && (x[0] == x[z]) && (y[0] == y[z])) {
                inGame = false; //koniec gry
            }
        }
         //sprawdzenie czy waz uderzyl w konce tablicy
        if (y[0] >= B_HEIGHT) {
            inGame = false; //jesli tak to zakoncz gre
        }

        if (y[0] < 0) {
            inGame = false;
        }

        if (x[0] >= B_WIDTH) {
            inGame = false;
        }

        if (x[0] < 0) {
            inGame = false;
        }

        if (!inGame) {
            timer.stop();   //zatrzymaj timer
        }
    }

    private void locateApple() {    //losowe ustawianie jablka

        int r = (int) (Math.random() * RAND_POS);   //liczba losowa * liczba pomocnicza
        apple_x = ((r * DOT_SIZE));     //wspolrzedna pomnozona jeszcze przez rozmiar kropki

        r = (int) (Math.random() * RAND_POS);
        apple_y = ((r * DOT_SIZE));
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if (inGame) { //jesli gra trwa

            checkApple();   //sprawdz czy waz zjadl jablko
            checkCollision();   //sprawdz czy nie uderzyl w ramke
            move(); //wykonuj ruchy
        }

        repaint();
    }

    private class TAdapter extends KeyAdapter {

        @Override
        public void keyPressed(KeyEvent e) {    //obsluga wciskanych przyciskow na klawiaturze

            int key = e.getKeyCode();   //pobranie wcisnietego przycisku

            if ((key == KeyEvent.VK_LEFT) && (!GoRight)) {  //klawisz skretu w lewo
                GoLeft = true;  //skret w lewo -> ture
                GoUp = false;
                GoDown = false;
            }

            if ((key == KeyEvent.VK_RIGHT) && (!GoLeft)) {  //klawisz skretu w prawo
                GoRight = true; //skret w prawo -> true
                GoUp = false;
                GoDown = false;
            }

            if ((key == KeyEvent.VK_UP) && (!GoDown)) { //klawisz skretu w gore
                GoUp = true;    //skret w gore -> true
                GoRight = false;
                GoLeft = false;
            }

            if ((key == KeyEvent.VK_DOWN) && (!GoUp)) { //klawisz skretu w dol
                GoDown = true;  //skret w dol -> true
                GoRight = false;
                GoLeft = false;
            }
        }
    }
}